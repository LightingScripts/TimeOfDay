﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[ExecuteInEditMode]
public class TimeOfDay : MonoBehaviour 
{

    //GameObjects
    public GameObject Sunlight;
    public Text debugTextUI;
    public AnimationCurve IntensityCurve;
    public Gradient ColorGradient;
    public GameObject Sunparent;

    // Time
    public bool bFixedTimeOfDay = false;
    private float fTimeOfDay = 0;
    public float fTimeMultiplier;
    public float fStartTime;
    private float fTimeFlow = 1;

    // Sunlight
    private float fSunXAngle = 90 ;
    public float lattitude = 45 ;
    public float yAngle = 0 ;
    private float InitialSunIntensity ;

    //Debug
    public bool bPrintIntensity = false;

    void OnGUI()
    {
        if (!Application.isPlaying)
        {
            //Reset rotations
            this.transform.rotation = Quaternion.identity;
            Sunparent.transform.rotation = Quaternion.identity;
            Sunlight.transform.rotation = Quaternion.identity;

            fTimeOfDay = fStartTime;
            fSunXAngle = (fStartTime / 24 * 360) - 90;

            //Apply rotations
            Sunparent.transform.Rotate(0, 0, lattitude);
            this.transform.Rotate( 0, yAngle, 0 );
            Sunlight.transform.Rotate(fSunXAngle, 0, 0);
        }
    }

    // Use this for initialization
    void Start () 
	{
		fTimeOfDay = fStartTime;
        InitialSunIntensity = Sunlight.GetComponent<Light>().intensity;
        //Sunparent.transform.Rotate(0, 0, lattitude);
    }
	
	// Update is called once per frame
	void Update () 
	{
		UpdateTimeOfDay ();
		UpdateSunIntensity ();
		UpdateSunRotation (fTimeFlow);
		UpdateSunYRotation ();
		UpdateUI ();
	}

	void UpdateTimeOfDay ()
	{
		if (bFixedTimeOfDay) 
		{
			if (Input.GetKey ("right")) 
			{
				fTimeOfDay = (fTimeOfDay + Time.fixedDeltaTime * fTimeMultiplier) % 24;
				fTimeFlow = 1 ;
			}
			if (Input.GetKey ("left")) 
			{
				fTimeOfDay = (fTimeOfDay - Time.fixedDeltaTime * fTimeMultiplier) % 24;
				fTimeFlow = -1 ;
			}
			if (!Input.GetKey ("right") && !Input.GetKey ("left")) 
			{
				fTimeFlow = 0;
			}
		}
		else 
		{
			if (fTimeOfDay >= 24) 
			{
				fTimeOfDay = 0;
				//UpdateSunRotation (1);
			} 
			else 
			{	
				fTimeOfDay = (fTimeOfDay + Time.fixedDeltaTime * fTimeMultiplier);
				//UpdateSunRotation (1);
			}
			print (fTimeOfDay);
		}
	}

	void UpdateSunIntensity ()
	{
        Sunlight.GetComponent<Light>().intensity = InitialSunIntensity * IntensityCurve.Evaluate(fTimeOfDay);
        Sunlight.GetComponent<Light>().color = ColorGradient.Evaluate(fTimeOfDay/24);

        if ( bPrintIntensity)
        {
            print(Sunlight.GetComponent<Light>().intensity);
        }
    }

	void UpdateSunRotation (float Multiplier = 1 )
	{
		float internalMultiplier = fTimeMultiplier * Multiplier;
		fSunXAngle = (Time.fixedDeltaTime * internalMultiplier / 24 * 360) ;
		Sunlight.transform.Rotate( fSunXAngle, 0, 0 );
	}

	void UpdateSunYRotation ()
	{
		if (Input.GetKey ("up")) 
		{
			this.transform.Rotate( 0, 1, 0 );
		}
		if (Input.GetKey ("down")) 
		{
			this.transform.Rotate (0, -1, 0);
		}
	}

	void UpdateUI ()
	{
		debugTextUI.text= "Time Of Day : "+ fTimeOfDay.ToString ();
	}
}